/* tslint:disable */
/* eslint-disable */
/**
 * management.proto
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: version not set
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface NlxmanagementOrganization
 */
export interface NlxmanagementOrganization {
    /**
     * 
     * @type {string}
     * @memberof NlxmanagementOrganization
     */
    serialNumber?: string;
    /**
     * 
     * @type {string}
     * @memberof NlxmanagementOrganization
     */
    name?: string;
}

export function NlxmanagementOrganizationFromJSON(json: any): NlxmanagementOrganization {
    return NlxmanagementOrganizationFromJSONTyped(json, false);
}

export function NlxmanagementOrganizationFromJSONTyped(json: any, ignoreDiscriminator: boolean): NlxmanagementOrganization {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'serialNumber': !exists(json, 'serialNumber') ? undefined : json['serialNumber'],
        'name': !exists(json, 'name') ? undefined : json['name'],
    };
}

export function NlxmanagementOrganizationToJSON(value?: NlxmanagementOrganization | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'serialNumber': value.serialNumber,
        'name': value.name,
    };
}


