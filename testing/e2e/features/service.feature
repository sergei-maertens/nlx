@management @service
Feature: Service

    Scenario: Create a service
        Given "Gemeente Stijns" is logged into NLX management
            And "Gemeenste Stijns" has an inway with the "Inway-01" running
        When "Gemeente Stijns" create a service with valid properties named "MyService" and as inway "Inway-01"
        Then the service "MyService" shows up under My services of the management interface of "Gemeente Stijns"

    @ignore
    Scenario: Revoke access to the service
        Given "RvRD" is logged into NLX management
            And "RvRD" has given access to "Gemeenste Stijns" for the service "voorbeeld-websockets"
            And the websocket chat of "Gemeente Stijns" can establish a connection
        When "RvRD" revokes access of "Gemeenste Stijns" to "voorbeeld-websockets"
        Then the websocket chat of "Gemeente Stijns" cannot establish a connection

    @ignore
    Scenario: Delete a service
        Given "Gemeente Stijns" is logged into NLX management
            And "Gemeente Stijns" offers the service "parkeerrechten" using the inway "gemeente-stijns-nlx-inway"
        When "Gemeente Stijns" removes the service "parkeerrechten"
        Then the service "parkeerrechten" is no longer available under My Services of the management interface of "Gemeente Stijns"
        
