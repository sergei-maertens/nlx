// Code generated by MockGen. DO NOT EDIT.
// Source: pkg/database/database.go

// Package mock_database is a generated GoMock package.
package mock_database

import (
	context "context"
	reflect "reflect"
	time "time"

	gomock "github.com/golang/mock/gomock"

	diagnostics "go.nlx.io/nlx/common/diagnostics"
	domain "go.nlx.io/nlx/management-api/domain"
	database "go.nlx.io/nlx/management-api/pkg/database"
)

// MockConfigDatabase is a mock of ConfigDatabase interface.
type MockConfigDatabase struct {
	ctrl     *gomock.Controller
	recorder *MockConfigDatabaseMockRecorder
}

// MockConfigDatabaseMockRecorder is the mock recorder for MockConfigDatabase.
type MockConfigDatabaseMockRecorder struct {
	mock *MockConfigDatabase
}

// NewMockConfigDatabase creates a new mock instance.
func NewMockConfigDatabase(ctrl *gomock.Controller) *MockConfigDatabase {
	mock := &MockConfigDatabase{ctrl: ctrl}
	mock.recorder = &MockConfigDatabaseMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockConfigDatabase) EXPECT() *MockConfigDatabaseMockRecorder {
	return m.recorder
}

// CreateAccessGrant mocks base method.
func (m *MockConfigDatabase) CreateAccessGrant(ctx context.Context, accessRequest *database.IncomingAccessRequest) (*database.AccessGrant, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateAccessGrant", ctx, accessRequest)
	ret0, _ := ret[0].(*database.AccessGrant)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateAccessGrant indicates an expected call of CreateAccessGrant.
func (mr *MockConfigDatabaseMockRecorder) CreateAccessGrant(ctx, accessRequest interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateAccessGrant", reflect.TypeOf((*MockConfigDatabase)(nil).CreateAccessGrant), ctx, accessRequest)
}

// CreateAccessProof mocks base method.
func (m *MockConfigDatabase) CreateAccessProof(ctx context.Context, accessRequest *database.OutgoingAccessRequest) (*database.AccessProof, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateAccessProof", ctx, accessRequest)
	ret0, _ := ret[0].(*database.AccessProof)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateAccessProof indicates an expected call of CreateAccessProof.
func (mr *MockConfigDatabaseMockRecorder) CreateAccessProof(ctx, accessRequest interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateAccessProof", reflect.TypeOf((*MockConfigDatabase)(nil).CreateAccessProof), ctx, accessRequest)
}

// CreateAuditLogRecord mocks base method.
func (m *MockConfigDatabase) CreateAuditLogRecord(ctx context.Context, auditLogRecord *database.AuditLog) (*database.AuditLog, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateAuditLogRecord", ctx, auditLogRecord)
	ret0, _ := ret[0].(*database.AuditLog)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateAuditLogRecord indicates an expected call of CreateAuditLogRecord.
func (mr *MockConfigDatabaseMockRecorder) CreateAuditLogRecord(ctx, auditLogRecord interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateAuditLogRecord", reflect.TypeOf((*MockConfigDatabase)(nil).CreateAuditLogRecord), ctx, auditLogRecord)
}

// CreateIncomingAccessRequest mocks base method.
func (m *MockConfigDatabase) CreateIncomingAccessRequest(ctx context.Context, accessRequest *database.IncomingAccessRequest) (*database.IncomingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateIncomingAccessRequest", ctx, accessRequest)
	ret0, _ := ret[0].(*database.IncomingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateIncomingAccessRequest indicates an expected call of CreateIncomingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) CreateIncomingAccessRequest(ctx, accessRequest interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateIncomingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).CreateIncomingAccessRequest), ctx, accessRequest)
}

// CreateOutgoingAccessRequest mocks base method.
func (m *MockConfigDatabase) CreateOutgoingAccessRequest(ctx context.Context, accessRequest *database.OutgoingAccessRequest) (*database.OutgoingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateOutgoingAccessRequest", ctx, accessRequest)
	ret0, _ := ret[0].(*database.OutgoingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateOutgoingAccessRequest indicates an expected call of CreateOutgoingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) CreateOutgoingAccessRequest(ctx, accessRequest interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateOutgoingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).CreateOutgoingAccessRequest), ctx, accessRequest)
}

// CreateOutgoingOrder mocks base method.
func (m *MockConfigDatabase) CreateOutgoingOrder(ctx context.Context, order *database.OutgoingOrder) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateOutgoingOrder", ctx, order)
	ret0, _ := ret[0].(error)
	return ret0
}

// CreateOutgoingOrder indicates an expected call of CreateOutgoingOrder.
func (mr *MockConfigDatabaseMockRecorder) CreateOutgoingOrder(ctx, order interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateOutgoingOrder", reflect.TypeOf((*MockConfigDatabase)(nil).CreateOutgoingOrder), ctx, order)
}

// CreateService mocks base method.
func (m *MockConfigDatabase) CreateService(ctx context.Context, service *database.Service) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateService", ctx, service)
	ret0, _ := ret[0].(error)
	return ret0
}

// CreateService indicates an expected call of CreateService.
func (mr *MockConfigDatabaseMockRecorder) CreateService(ctx, service interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateService", reflect.TypeOf((*MockConfigDatabase)(nil).CreateService), ctx, service)
}

// CreateServiceWithInways mocks base method.
func (m *MockConfigDatabase) CreateServiceWithInways(ctx context.Context, service *database.Service, inwayNames []string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateServiceWithInways", ctx, service, inwayNames)
	ret0, _ := ret[0].(error)
	return ret0
}

// CreateServiceWithInways indicates an expected call of CreateServiceWithInways.
func (mr *MockConfigDatabaseMockRecorder) CreateServiceWithInways(ctx, service, inwayNames interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateServiceWithInways", reflect.TypeOf((*MockConfigDatabase)(nil).CreateServiceWithInways), ctx, service, inwayNames)
}

// CreateUser mocks base method.
func (m *MockConfigDatabase) CreateUser(ctx context.Context, email, password string, roleNames []string) (*database.User, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateUser", ctx, email, password, roleNames)
	ret0, _ := ret[0].(*database.User)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateUser indicates an expected call of CreateUser.
func (mr *MockConfigDatabaseMockRecorder) CreateUser(ctx, email, password, roleNames interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateUser", reflect.TypeOf((*MockConfigDatabase)(nil).CreateUser), ctx, email, password, roleNames)
}

// DeleteInway mocks base method.
func (m *MockConfigDatabase) DeleteInway(ctx context.Context, name string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteInway", ctx, name)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteInway indicates an expected call of DeleteInway.
func (mr *MockConfigDatabaseMockRecorder) DeleteInway(ctx, name interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteInway", reflect.TypeOf((*MockConfigDatabase)(nil).DeleteInway), ctx, name)
}

// DeleteOutgoingAccessRequests mocks base method.
func (m *MockConfigDatabase) DeleteOutgoingAccessRequests(ctx context.Context, organizationSerialNumber, serviceName string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteOutgoingAccessRequests", ctx, organizationSerialNumber, serviceName)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteOutgoingAccessRequests indicates an expected call of DeleteOutgoingAccessRequests.
func (mr *MockConfigDatabaseMockRecorder) DeleteOutgoingAccessRequests(ctx, organizationSerialNumber, serviceName interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteOutgoingAccessRequests", reflect.TypeOf((*MockConfigDatabase)(nil).DeleteOutgoingAccessRequests), ctx, organizationSerialNumber, serviceName)
}

// DeleteService mocks base method.
func (m *MockConfigDatabase) DeleteService(ctx context.Context, serviceName, organizationSerialNumber string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteService", ctx, serviceName, organizationSerialNumber)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteService indicates an expected call of DeleteService.
func (mr *MockConfigDatabaseMockRecorder) DeleteService(ctx, serviceName, organizationSerialNumber interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteService", reflect.TypeOf((*MockConfigDatabase)(nil).DeleteService), ctx, serviceName, organizationSerialNumber)
}

// GetAccessGrant mocks base method.
func (m *MockConfigDatabase) GetAccessGrant(ctx context.Context, id uint) (*database.AccessGrant, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAccessGrant", ctx, id)
	ret0, _ := ret[0].(*database.AccessGrant)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAccessGrant indicates an expected call of GetAccessGrant.
func (mr *MockConfigDatabaseMockRecorder) GetAccessGrant(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAccessGrant", reflect.TypeOf((*MockConfigDatabase)(nil).GetAccessGrant), ctx, id)
}

// GetAccessProofForOutgoingAccessRequest mocks base method.
func (m *MockConfigDatabase) GetAccessProofForOutgoingAccessRequest(ctx context.Context, accessRequestID uint) (*database.AccessProof, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAccessProofForOutgoingAccessRequest", ctx, accessRequestID)
	ret0, _ := ret[0].(*database.AccessProof)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAccessProofForOutgoingAccessRequest indicates an expected call of GetAccessProofForOutgoingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) GetAccessProofForOutgoingAccessRequest(ctx, accessRequestID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAccessProofForOutgoingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).GetAccessProofForOutgoingAccessRequest), ctx, accessRequestID)
}

// GetIncomingAccessRequest mocks base method.
func (m *MockConfigDatabase) GetIncomingAccessRequest(ctx context.Context, id uint) (*database.IncomingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetIncomingAccessRequest", ctx, id)
	ret0, _ := ret[0].(*database.IncomingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetIncomingAccessRequest indicates an expected call of GetIncomingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) GetIncomingAccessRequest(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetIncomingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).GetIncomingAccessRequest), ctx, id)
}

// GetIncomingAccessRequestCountByService mocks base method.
func (m *MockConfigDatabase) GetIncomingAccessRequestCountByService(ctx context.Context) (map[string]int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetIncomingAccessRequestCountByService", ctx)
	ret0, _ := ret[0].(map[string]int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetIncomingAccessRequestCountByService indicates an expected call of GetIncomingAccessRequestCountByService.
func (mr *MockConfigDatabaseMockRecorder) GetIncomingAccessRequestCountByService(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetIncomingAccessRequestCountByService", reflect.TypeOf((*MockConfigDatabase)(nil).GetIncomingAccessRequestCountByService), ctx)
}

// GetInway mocks base method.
func (m *MockConfigDatabase) GetInway(ctx context.Context, name string) (*database.Inway, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetInway", ctx, name)
	ret0, _ := ret[0].(*database.Inway)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetInway indicates an expected call of GetInway.
func (mr *MockConfigDatabaseMockRecorder) GetInway(ctx, name interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetInway", reflect.TypeOf((*MockConfigDatabase)(nil).GetInway), ctx, name)
}

// GetLatestAccessGrantForService mocks base method.
func (m *MockConfigDatabase) GetLatestAccessGrantForService(ctx context.Context, organizationSerialNumber, serviceName string) (*database.AccessGrant, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetLatestAccessGrantForService", ctx, organizationSerialNumber, serviceName)
	ret0, _ := ret[0].(*database.AccessGrant)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetLatestAccessGrantForService indicates an expected call of GetLatestAccessGrantForService.
func (mr *MockConfigDatabaseMockRecorder) GetLatestAccessGrantForService(ctx, organizationSerialNumber, serviceName interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetLatestAccessGrantForService", reflect.TypeOf((*MockConfigDatabase)(nil).GetLatestAccessGrantForService), ctx, organizationSerialNumber, serviceName)
}

// GetLatestIncomingAccessRequest mocks base method.
func (m *MockConfigDatabase) GetLatestIncomingAccessRequest(ctx context.Context, organizationSerialNumber, serviceName string) (*database.IncomingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetLatestIncomingAccessRequest", ctx, organizationSerialNumber, serviceName)
	ret0, _ := ret[0].(*database.IncomingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetLatestIncomingAccessRequest indicates an expected call of GetLatestIncomingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) GetLatestIncomingAccessRequest(ctx, organizationSerialNumber, serviceName interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetLatestIncomingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).GetLatestIncomingAccessRequest), ctx, organizationSerialNumber, serviceName)
}

// GetLatestOutgoingAccessRequest mocks base method.
func (m *MockConfigDatabase) GetLatestOutgoingAccessRequest(ctx context.Context, organizationSerialNumber, serviceName string) (*database.OutgoingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetLatestOutgoingAccessRequest", ctx, organizationSerialNumber, serviceName)
	ret0, _ := ret[0].(*database.OutgoingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetLatestOutgoingAccessRequest indicates an expected call of GetLatestOutgoingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) GetLatestOutgoingAccessRequest(ctx, organizationSerialNumber, serviceName interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetLatestOutgoingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).GetLatestOutgoingAccessRequest), ctx, organizationSerialNumber, serviceName)
}

// GetOutgoingAccessRequest mocks base method.
func (m *MockConfigDatabase) GetOutgoingAccessRequest(ctx context.Context, id uint) (*database.OutgoingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetOutgoingAccessRequest", ctx, id)
	ret0, _ := ret[0].(*database.OutgoingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetOutgoingAccessRequest indicates an expected call of GetOutgoingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) GetOutgoingAccessRequest(ctx, id interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetOutgoingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).GetOutgoingAccessRequest), ctx, id)
}

// GetOutgoingOrderByReference mocks base method.
func (m *MockConfigDatabase) GetOutgoingOrderByReference(ctx context.Context, reference string) (*database.OutgoingOrder, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetOutgoingOrderByReference", ctx, reference)
	ret0, _ := ret[0].(*database.OutgoingOrder)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetOutgoingOrderByReference indicates an expected call of GetOutgoingOrderByReference.
func (mr *MockConfigDatabaseMockRecorder) GetOutgoingOrderByReference(ctx, reference interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetOutgoingOrderByReference", reflect.TypeOf((*MockConfigDatabase)(nil).GetOutgoingOrderByReference), ctx, reference)
}

// GetOutway mocks base method.
func (m *MockConfigDatabase) GetOutway(ctx context.Context, name string) (*database.Outway, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetOutway", ctx, name)
	ret0, _ := ret[0].(*database.Outway)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetOutway indicates an expected call of GetOutway.
func (mr *MockConfigDatabaseMockRecorder) GetOutway(ctx, name interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetOutway", reflect.TypeOf((*MockConfigDatabase)(nil).GetOutway), ctx, name)
}

// GetService mocks base method.
func (m *MockConfigDatabase) GetService(ctx context.Context, name string) (*database.Service, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetService", ctx, name)
	ret0, _ := ret[0].(*database.Service)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetService indicates an expected call of GetService.
func (mr *MockConfigDatabaseMockRecorder) GetService(ctx, name interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetService", reflect.TypeOf((*MockConfigDatabase)(nil).GetService), ctx, name)
}

// GetSettings mocks base method.
func (m *MockConfigDatabase) GetSettings(ctx context.Context) (*domain.Settings, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSettings", ctx)
	ret0, _ := ret[0].(*domain.Settings)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSettings indicates an expected call of GetSettings.
func (mr *MockConfigDatabaseMockRecorder) GetSettings(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSettings", reflect.TypeOf((*MockConfigDatabase)(nil).GetSettings), ctx)
}

// GetUser mocks base method.
func (m *MockConfigDatabase) GetUser(ctx context.Context, email string) (*database.User, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetUser", ctx, email)
	ret0, _ := ret[0].(*database.User)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUser indicates an expected call of GetUser.
func (mr *MockConfigDatabaseMockRecorder) GetUser(ctx, email interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUser", reflect.TypeOf((*MockConfigDatabase)(nil).GetUser), ctx, email)
}

// ListAccessGrantsForService mocks base method.
func (m *MockConfigDatabase) ListAccessGrantsForService(ctx context.Context, serviceName string) ([]*database.AccessGrant, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListAccessGrantsForService", ctx, serviceName)
	ret0, _ := ret[0].([]*database.AccessGrant)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListAccessGrantsForService indicates an expected call of ListAccessGrantsForService.
func (mr *MockConfigDatabaseMockRecorder) ListAccessGrantsForService(ctx, serviceName interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListAccessGrantsForService", reflect.TypeOf((*MockConfigDatabase)(nil).ListAccessGrantsForService), ctx, serviceName)
}

// ListAllIncomingAccessRequests mocks base method.
func (m *MockConfigDatabase) ListAllIncomingAccessRequests(ctx context.Context) ([]*database.IncomingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListAllIncomingAccessRequests", ctx)
	ret0, _ := ret[0].([]*database.IncomingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListAllIncomingAccessRequests indicates an expected call of ListAllIncomingAccessRequests.
func (mr *MockConfigDatabaseMockRecorder) ListAllIncomingAccessRequests(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListAllIncomingAccessRequests", reflect.TypeOf((*MockConfigDatabase)(nil).ListAllIncomingAccessRequests), ctx)
}

// ListAuditLogRecords mocks base method.
func (m *MockConfigDatabase) ListAuditLogRecords(ctx context.Context) ([]*database.AuditLog, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListAuditLogRecords", ctx)
	ret0, _ := ret[0].([]*database.AuditLog)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListAuditLogRecords indicates an expected call of ListAuditLogRecords.
func (mr *MockConfigDatabaseMockRecorder) ListAuditLogRecords(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListAuditLogRecords", reflect.TypeOf((*MockConfigDatabase)(nil).ListAuditLogRecords), ctx)
}

// ListIncomingOrders mocks base method.
func (m *MockConfigDatabase) ListIncomingOrders(ctx context.Context) ([]*domain.IncomingOrder, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListIncomingOrders", ctx)
	ret0, _ := ret[0].([]*domain.IncomingOrder)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListIncomingOrders indicates an expected call of ListIncomingOrders.
func (mr *MockConfigDatabaseMockRecorder) ListIncomingOrders(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListIncomingOrders", reflect.TypeOf((*MockConfigDatabase)(nil).ListIncomingOrders), ctx)
}

// ListInways mocks base method.
func (m *MockConfigDatabase) ListInways(ctx context.Context) ([]*database.Inway, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListInways", ctx)
	ret0, _ := ret[0].([]*database.Inway)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListInways indicates an expected call of ListInways.
func (mr *MockConfigDatabaseMockRecorder) ListInways(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListInways", reflect.TypeOf((*MockConfigDatabase)(nil).ListInways), ctx)
}

// ListOutgoingOrders mocks base method.
func (m *MockConfigDatabase) ListOutgoingOrders(ctx context.Context) ([]*database.OutgoingOrder, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListOutgoingOrders", ctx)
	ret0, _ := ret[0].([]*database.OutgoingOrder)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListOutgoingOrders indicates an expected call of ListOutgoingOrders.
func (mr *MockConfigDatabaseMockRecorder) ListOutgoingOrders(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListOutgoingOrders", reflect.TypeOf((*MockConfigDatabase)(nil).ListOutgoingOrders), ctx)
}

// ListOutgoingOrdersByOrganization mocks base method.
func (m *MockConfigDatabase) ListOutgoingOrdersByOrganization(ctx context.Context, organizationSerialNumber string) ([]*database.OutgoingOrder, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListOutgoingOrdersByOrganization", ctx, organizationSerialNumber)
	ret0, _ := ret[0].([]*database.OutgoingOrder)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListOutgoingOrdersByOrganization indicates an expected call of ListOutgoingOrdersByOrganization.
func (mr *MockConfigDatabaseMockRecorder) ListOutgoingOrdersByOrganization(ctx, organizationSerialNumber interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListOutgoingOrdersByOrganization", reflect.TypeOf((*MockConfigDatabase)(nil).ListOutgoingOrdersByOrganization), ctx, organizationSerialNumber)
}

// ListOutways mocks base method.
func (m *MockConfigDatabase) ListOutways(ctx context.Context) ([]*database.Outway, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListOutways", ctx)
	ret0, _ := ret[0].([]*database.Outway)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListOutways indicates an expected call of ListOutways.
func (mr *MockConfigDatabaseMockRecorder) ListOutways(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListOutways", reflect.TypeOf((*MockConfigDatabase)(nil).ListOutways), ctx)
}

// ListServices mocks base method.
func (m *MockConfigDatabase) ListServices(ctx context.Context) ([]*database.Service, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListServices", ctx)
	ret0, _ := ret[0].([]*database.Service)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListServices indicates an expected call of ListServices.
func (mr *MockConfigDatabaseMockRecorder) ListServices(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListServices", reflect.TypeOf((*MockConfigDatabase)(nil).ListServices), ctx)
}

// RegisterInway mocks base method.
func (m *MockConfigDatabase) RegisterInway(ctx context.Context, inway *database.Inway) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RegisterInway", ctx, inway)
	ret0, _ := ret[0].(error)
	return ret0
}

// RegisterInway indicates an expected call of RegisterInway.
func (mr *MockConfigDatabaseMockRecorder) RegisterInway(ctx, inway interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RegisterInway", reflect.TypeOf((*MockConfigDatabase)(nil).RegisterInway), ctx, inway)
}

// RegisterOutway mocks base method.
func (m *MockConfigDatabase) RegisterOutway(ctx context.Context, outway *database.Outway) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RegisterOutway", ctx, outway)
	ret0, _ := ret[0].(error)
	return ret0
}

// RegisterOutway indicates an expected call of RegisterOutway.
func (mr *MockConfigDatabaseMockRecorder) RegisterOutway(ctx, outway interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RegisterOutway", reflect.TypeOf((*MockConfigDatabase)(nil).RegisterOutway), ctx, outway)
}

// RevokeAccessGrant mocks base method.
func (m *MockConfigDatabase) RevokeAccessGrant(ctx context.Context, id uint, revokedAt time.Time) (*database.AccessGrant, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RevokeAccessGrant", ctx, id, revokedAt)
	ret0, _ := ret[0].(*database.AccessGrant)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// RevokeAccessGrant indicates an expected call of RevokeAccessGrant.
func (mr *MockConfigDatabaseMockRecorder) RevokeAccessGrant(ctx, id, revokedAt interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RevokeAccessGrant", reflect.TypeOf((*MockConfigDatabase)(nil).RevokeAccessGrant), ctx, id, revokedAt)
}

// RevokeAccessProof mocks base method.
func (m *MockConfigDatabase) RevokeAccessProof(ctx context.Context, id uint, revokedAt time.Time) (*database.AccessProof, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RevokeAccessProof", ctx, id, revokedAt)
	ret0, _ := ret[0].(*database.AccessProof)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// RevokeAccessProof indicates an expected call of RevokeAccessProof.
func (mr *MockConfigDatabaseMockRecorder) RevokeAccessProof(ctx, id, revokedAt interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RevokeAccessProof", reflect.TypeOf((*MockConfigDatabase)(nil).RevokeAccessProof), ctx, id, revokedAt)
}

// RevokeOutgoingOrderByReference mocks base method.
func (m *MockConfigDatabase) RevokeOutgoingOrderByReference(ctx context.Context, delegatee, reference string, revokedAt time.Time) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RevokeOutgoingOrderByReference", ctx, delegatee, reference, revokedAt)
	ret0, _ := ret[0].(error)
	return ret0
}

// RevokeOutgoingOrderByReference indicates an expected call of RevokeOutgoingOrderByReference.
func (mr *MockConfigDatabaseMockRecorder) RevokeOutgoingOrderByReference(ctx, delegatee, reference, revokedAt interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RevokeOutgoingOrderByReference", reflect.TypeOf((*MockConfigDatabase)(nil).RevokeOutgoingOrderByReference), ctx, delegatee, reference, revokedAt)
}

// SynchronizeOrders mocks base method.
func (m *MockConfigDatabase) SynchronizeOrders(ctx context.Context, orders []*database.IncomingOrder) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SynchronizeOrders", ctx, orders)
	ret0, _ := ret[0].(error)
	return ret0
}

// SynchronizeOrders indicates an expected call of SynchronizeOrders.
func (mr *MockConfigDatabaseMockRecorder) SynchronizeOrders(ctx, orders interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SynchronizeOrders", reflect.TypeOf((*MockConfigDatabase)(nil).SynchronizeOrders), ctx, orders)
}

// TakePendingOutgoingAccessRequest mocks base method.
func (m *MockConfigDatabase) TakePendingOutgoingAccessRequest(ctx context.Context) (*database.OutgoingAccessRequest, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "TakePendingOutgoingAccessRequest", ctx)
	ret0, _ := ret[0].(*database.OutgoingAccessRequest)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// TakePendingOutgoingAccessRequest indicates an expected call of TakePendingOutgoingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) TakePendingOutgoingAccessRequest(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "TakePendingOutgoingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).TakePendingOutgoingAccessRequest), ctx)
}

// UnlockOutgoingAccessRequest mocks base method.
func (m *MockConfigDatabase) UnlockOutgoingAccessRequest(ctx context.Context, accessRequest *database.OutgoingAccessRequest) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UnlockOutgoingAccessRequest", ctx, accessRequest)
	ret0, _ := ret[0].(error)
	return ret0
}

// UnlockOutgoingAccessRequest indicates an expected call of UnlockOutgoingAccessRequest.
func (mr *MockConfigDatabaseMockRecorder) UnlockOutgoingAccessRequest(ctx, accessRequest interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UnlockOutgoingAccessRequest", reflect.TypeOf((*MockConfigDatabase)(nil).UnlockOutgoingAccessRequest), ctx, accessRequest)
}

// UpdateIncomingAccessRequestState mocks base method.
func (m *MockConfigDatabase) UpdateIncomingAccessRequestState(ctx context.Context, id uint, state database.IncomingAccessRequestState) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateIncomingAccessRequestState", ctx, id, state)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateIncomingAccessRequestState indicates an expected call of UpdateIncomingAccessRequestState.
func (mr *MockConfigDatabaseMockRecorder) UpdateIncomingAccessRequestState(ctx, id, state interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateIncomingAccessRequestState", reflect.TypeOf((*MockConfigDatabase)(nil).UpdateIncomingAccessRequestState), ctx, id, state)
}

// UpdateInway mocks base method.
func (m *MockConfigDatabase) UpdateInway(ctx context.Context, inway *database.Inway) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateInway", ctx, inway)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateInway indicates an expected call of UpdateInway.
func (mr *MockConfigDatabaseMockRecorder) UpdateInway(ctx, inway interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateInway", reflect.TypeOf((*MockConfigDatabase)(nil).UpdateInway), ctx, inway)
}

// UpdateOutgoingAccessRequestState mocks base method.
func (m *MockConfigDatabase) UpdateOutgoingAccessRequestState(ctx context.Context, id uint, state database.OutgoingAccessRequestState, referenceID uint, err *diagnostics.ErrorDetails) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateOutgoingAccessRequestState", ctx, id, state, referenceID, err)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateOutgoingAccessRequestState indicates an expected call of UpdateOutgoingAccessRequestState.
func (mr *MockConfigDatabaseMockRecorder) UpdateOutgoingAccessRequestState(ctx, id, state, referenceID, err interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateOutgoingAccessRequestState", reflect.TypeOf((*MockConfigDatabase)(nil).UpdateOutgoingAccessRequestState), ctx, id, state, referenceID, err)
}

// UpdateOutgoingOrder mocks base method.
func (m *MockConfigDatabase) UpdateOutgoingOrder(ctx context.Context, order *database.OutgoingOrder) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateOutgoingOrder", ctx, order)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateOutgoingOrder indicates an expected call of UpdateOutgoingOrder.
func (mr *MockConfigDatabaseMockRecorder) UpdateOutgoingOrder(ctx, order interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateOutgoingOrder", reflect.TypeOf((*MockConfigDatabase)(nil).UpdateOutgoingOrder), ctx, order)
}

// UpdateService mocks base method.
func (m *MockConfigDatabase) UpdateService(ctx context.Context, service *database.Service) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateService", ctx, service)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateService indicates an expected call of UpdateService.
func (mr *MockConfigDatabaseMockRecorder) UpdateService(ctx, service interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateService", reflect.TypeOf((*MockConfigDatabase)(nil).UpdateService), ctx, service)
}

// UpdateServiceWithInways mocks base method.
func (m *MockConfigDatabase) UpdateServiceWithInways(ctx context.Context, service *database.Service, inwayNames []string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateServiceWithInways", ctx, service, inwayNames)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateServiceWithInways indicates an expected call of UpdateServiceWithInways.
func (mr *MockConfigDatabaseMockRecorder) UpdateServiceWithInways(ctx, service, inwayNames interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateServiceWithInways", reflect.TypeOf((*MockConfigDatabase)(nil).UpdateServiceWithInways), ctx, service, inwayNames)
}

// UpdateSettings mocks base method.
func (m *MockConfigDatabase) UpdateSettings(ctx context.Context, settings *domain.Settings) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateSettings", ctx, settings)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateSettings indicates an expected call of UpdateSettings.
func (mr *MockConfigDatabaseMockRecorder) UpdateSettings(ctx, settings interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateSettings", reflect.TypeOf((*MockConfigDatabase)(nil).UpdateSettings), ctx, settings)
}

// VerifyUserCredentials mocks base method.
func (m *MockConfigDatabase) VerifyUserCredentials(ctx context.Context, email, password string) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "VerifyUserCredentials", ctx, email, password)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// VerifyUserCredentials indicates an expected call of VerifyUserCredentials.
func (mr *MockConfigDatabaseMockRecorder) VerifyUserCredentials(ctx, email, password interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "VerifyUserCredentials", reflect.TypeOf((*MockConfigDatabase)(nil).VerifyUserCredentials), ctx, email, password)
}
