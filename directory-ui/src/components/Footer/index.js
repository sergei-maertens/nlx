// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'

import { Container } from '../Grid'
import { FooterContent, Wrapper, StyledLogoVng } from './index.styles'

const Footer = () => (
  <Wrapper>
    <Container>
      <FooterContent>
        <StyledLogoVng />
      </FooterContent>
    </Container>
  </Wrapper>
)

export default Footer
