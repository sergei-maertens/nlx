// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const ContactLink = styled.a`
  margin-left: -2px;
`
