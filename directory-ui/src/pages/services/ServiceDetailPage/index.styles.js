// Copyright © VNG Realisatie 2018
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Drawer } from '@commonground/design-system'

export const StyledDrawer = styled(Drawer)`
  z-index: 100;
`
